import os
from jinja2 import Environment, PackageLoader, select_autoescape


class EnvironConfig:
    APP_ENVIRONMENT = os.environ.get("APP_ENVIRONMENT", "DEVELOPMENT")
    FLASK_APP_SECRET = os.environ.get("FLASK_APP_SECRET", "QhaP8AAE_32nFYH35N551bNH")
    