from application.configs.db import DbDevelopmentConfig
from application.configs.environ import EnvironConfig


class Config:
    SQLALCHEMY_DATABASE_URI = DbDevelopmentConfig.DB_CONNECTION_STRING
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET = "3kj$&sJK#q12"
    